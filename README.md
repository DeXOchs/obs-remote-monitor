# OBS Remote Monitor

Browser monitor for [OBS](https://obsproject.com/) which displays a preview of a specified OBS scene. Build with npm, svelte and Bulma.

## Requirements:

- [OBS](https://obsproject.com/) v28 or higher - this includes the latest version of the OBS-websocket plugin
- Enabling the OBS-websocket server in OBS under `Tools -> obs-websocket Settings -> Enable WebSocket Server`
- Optionally: a tunnel service if you want to control OBS outside your local network, [see these instructions](https://github.com/obsproject/obs-websocket/blob/4.x-compat/SSL-TUNNELLING.md)

---

## Build Instructions:

```bash
npm ci
npm run dev # or: npm run build
```