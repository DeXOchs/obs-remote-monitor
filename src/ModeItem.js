export default class ModeItem {
    constructor(name, friendlyName, exec) {
      this.name = name;
      this.friendlyName = friendlyName
      this.exec = exec
    }
  }